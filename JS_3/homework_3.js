//*** Описать своими словами для чего вообще нужны функции в программировании.

        // ОТВЕТ: Для того, чтобы не писать одно и то же по 100 раз, а записать данные в функцию и вызывать ее в нужных местах.
        // ***Описать своими словами, зачем в функцию передавать аргумент.

        // ОТВЕТ: Чтобы вызывать функции с разными аргументами, и использовать их заново,
        // а не создавать все время новые, одноразовые. Таким образом можно уменьшить код.
        
        // Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек(типа Jquery).

        // Технические требования:

        // Считать с помощью модального окна браузера два числа.
        // Считать с помощью модального окна браузера математическую операцию, которую нужно совершить.Сюда может быть введено +, -, *, /.
        // Создать функцию, в которую передать два значения и операцию.
        // Вывести в консоль результат выполнения функции.


        // Необязательное задание продвинутой сложности:

        // После ввода данных добавить проверку их корректности.Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново(при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).
        let userNumberOne = +prompt('Введите первое число');
        while (!isValidNumber(userNumberOne)) {
            userNumberOne = +prompt('Ввод некорректный, введите первое число еще раз');
        }
        let userNumberTwo = +prompt('Введите второе число');
        while (!isValidNumber(userNumberTwo)) {
            userNumberTwo = +prompt('Ввод некорректный, введите второе число еще раз');
        }
        let userAction = prompt('Выберите математическую операцию + - * /');
        console.log(userNumberOne, userNumberTwo,userAction )
        function countAction(numberOne, numberTwo, userAction) {
            switch (userAction) {
                case '+':
                    return (numberOne + numberTwo);
                    break;
                case '-':
                    return (numberOne - numberTwo);
                    break;
                case '*':
                    return (numberOne* numberTwo);
                    break;
                case '/':
                    return (numberOne/numberTwo);
            }
        }
        console.log (countAction(userNumberOne, userNumberTwo, userAction));
        // function addition(a, b) {
        //     return a + b;
        // }
        // function difference(a, b) {
        //     return a - b;
        // }
        // function multiplication(a, b) {
        //     return a * b;
        // }
        // function division(a, b) {
        //     return a / b;
        // }
        function isValidNumber(value) {
            if (value === "" || value === null || Number.isNaN(+value)) {
                return false;
            }
            return true;
        }